#include <iostream>
#include <conio.h>
#include <string>
#include <time.h>

using namespace std;

void randomizeValues(int numbers[]) {
	for (int x = 0; x != 10; x++) {
		numbers[x] = rand() % 69 + 1;
		cout << numbers[x] << endl;
	}
}

void bubbleSort(int numbers[]) {
	int choice;
	cout << "In what order do you want to sort it?" << endl;
	cout << "[1] Ascending order\n" << "[2] Descending order" << endl;
	cin >> choice;
	switch (choice) {
		case 1:
			cout << "Ascending Order" << endl;
			for (int x = 0; x < 10; x++) {
				for (int y = 0; y < (10 - x - 1); y++) {
					if (numbers[y] > numbers[y + 1]) {
						swap(numbers[y], numbers[y + 1]);
					}
				}
			}
			for (int x = 0; x < 10; x++) {
				cout << numbers[x] << endl;
			}
			break;
		case 2: 
			cout << "Descending Order" << endl;
			for (int x = 0; x < 10; x++) {
				for (int y = 0; y < (10 - x - 1); y++) {
					if (numbers[y] < numbers[y + 1]) {
						swap(numbers[y], numbers[y + 1]);
					}
				}
			}	
			for (int x = 0; x < 10; x++) {
				cout << numbers[x] << endl;
			}
			break;
	}
}

void selectionSort(int numbers[]) {
	int choice;
	cout << "In what order do you want to sort it?" << endl;
	cout << "[1] Ascending order\n" << "[2] Descending order" << endl;
	cin >> choice;
	switch (choice) {
		case 1:
			cout << "Ascending Order" << endl;
			for (int x = 0; x < 10; x++) {
				for (int y = 0; y < 10 - 1; y++) {
					if (numbers[y] > numbers[x]) {
						swap(numbers[y], numbers[x]);
					}
				}
			}
			for (int x = 0; x < 10; x++) {
				cout << numbers[x] << endl;
			}
			break;
	
		case 2:
			cout << "Descending Order" << endl;
			for (int x = 0; x < 10; x++) {
				for (int y = 0; y < 10 - 1; y++) {
					if (numbers[y] < numbers[x]) {
						swap(numbers[y], numbers[x]);
					}
				}
			}
			for (int x = 0; x < 10; x++) {
				cout << numbers[x] << endl;
			}
			break;
	}
}

void search(int numbers[]) {
	int value;	
	cout << "What value would you like to see? ";
	cin >> value;
	for (int x = 0; x < 10; x++) {
		if (numbers[x] == value) {
			cout << "No. of searches: " << x + 1 << endl;
			break;
		}
	}
}
int main()
{
	srand(time(NULL));
	int numbers[10];
	int choice, continueSort;
	randomizeValues(numbers);
	while (true) {
		cout << "what type of sorting would you like to use? " << endl;
		cout << "[1] Bubble Sort\n" << "[2] Selection Sort" << endl;
		cin >> choice;
		switch (choice) {
		case 1:
			bubbleSort(numbers);
			cout << "-----------------------------------------" << endl;
			search(numbers);
			break;
		case 2:
			selectionSort(numbers);
			cout << "-----------------------------------------" << endl;
			search(numbers);
			break;
		default:
			cout << "No such sorting exists!" << endl;
			break;
		}
		cout << "Do you still want to sort?" << endl;
		cout << "[1] Yes\n" << "[2] No" << endl;
		cin >> continueSort;
		switch (continueSort) {
			case 1:
				system("cls");
				continue;
			case 2:
				return 0;
		}
	}
}